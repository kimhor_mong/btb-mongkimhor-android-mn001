package com.example.miniproject;

import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.RadioButton;

import com.example.miniproject.databinding.ActivityProfileBinding;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class ProfileActivity extends AppCompatActivity {
    private ActivityProfileBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // initialize binding
        binding = ActivityProfileBinding.inflate(getLayoutInflater());

        // define view
        View view = binding.getRoot();

        // set view into content view
        setContentView(view);

        onComponentsInitialized();
        onButtonSaveClick();
        onButtonSelectDateClicked();
    }

    private void onComponentsInitialized() {
        String[] categories = {
                "Web Developer",
                "Mobile Developer",
                "Youtuber",
                "Data Analyst",
                "System Analyst"
        };
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>
                (this, R.layout.support_simple_spinner_dropdown_item, categories);

        binding.spinnerSkill.setAdapter(arrayAdapter);
    }

    private void onButtonSelectDateClicked() {
        final Calendar calendar = Calendar.getInstance();
        DatePickerDialog.OnDateSetListener date = (datePicker, year, monthOfYear, dayOfMonth) -> {
            calendar.set(Calendar.YEAR, year);
            calendar.set(Calendar.MONTH, monthOfYear);
            calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

            String myFormat = "dd/MM/yyyy";
            SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
            binding.labelBirthDay.setText(sdf.format(calendar.getTime()));
        };

        binding.btnSelectDate.setOnClickListener(v ->
                new DatePickerDialog(ProfileActivity.this, date, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH),
                calendar.get(Calendar.DAY_OF_MONTH)).show()
        );
    }

    private void onButtonSaveClick() {
       binding.btnSavePersonInfo.setOnClickListener(view -> {
           PersonInfo personInfo = getPersonInfo();
           if(isValidPersonInfo(personInfo)){
               binding.btnSelectDate.setError(null);
               Intent intent = new Intent(ProfileActivity.this, PersonInfoActivity.class);
               intent.putExtra("personInfo", personInfo);
               startActivity(intent);
           }
       });
    }

    private PersonInfo getPersonInfo(){
        int selectedChildId = binding.radioGroupGender.getCheckedRadioButtonId();
        RadioButton selectedGender = findViewById(selectedChildId);

        return new PersonInfo(
                binding.editTextName.getText().toString(),
                selectedGender.getText().toString(),
                binding.labelBirthDay.getText().toString(),
                binding.spinnerSkill.getSelectedItem().toString()
        );
    }

    private boolean isValidPersonInfo(PersonInfo personInfo){
        boolean isValid = true;

        if(personInfo.getName().length() == 0){
            binding.editTextName.setError("Name is required");
            isValid = false;
        }

        String labelBirthDay = getResources().getString(R.string.label_birthday);

        if(personInfo.getBirthday().length() == labelBirthDay.length()){
            binding.btnSelectDate.setError("Birthday is required");
            isValid = false;
        }
        return  isValid;
    }


}

