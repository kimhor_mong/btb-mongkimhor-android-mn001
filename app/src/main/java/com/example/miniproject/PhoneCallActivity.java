package com.example.miniproject;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;

import com.example.miniproject.databinding.ActivityPhoneCallBinding;

public class PhoneCallActivity extends AppCompatActivity {
    private ActivityPhoneCallBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // initialize binding
        binding = ActivityPhoneCallBinding.inflate(getLayoutInflater());

        // define view
        View view = binding.getRoot();

        // set view into content view
        setContentView(view);

        // methods for handling click event
        onButtonCallClicked();
    }

    private void onButtonCallClicked(){
        binding.btnCallNow.setOnClickListener(view -> {
            if(phoneNumberIsNotEmpty()){
                Intent intent = new Intent(Intent.ACTION_DIAL,
                        Uri.fromParts("tel", binding.editTextPhoneNumber.getText().toString(), null));
                startActivity(intent);
            } else {
                binding.editTextPhoneNumber.setError("Phone number is empty");
            }
        });

    }

    private boolean phoneNumberIsNotEmpty(){
        return binding.editTextPhoneNumber.getText().toString().length() > 0;
    }
}