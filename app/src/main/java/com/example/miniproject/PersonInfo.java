package com.example.miniproject;

import java.io.Serializable;

public class PersonInfo implements Serializable {
    private String name;
    private String gender;
    private String birthday;
    private String skill;

    public PersonInfo(String name, String gender, String birthday, String skill) {
        this.name = name;
        this.gender = gender;
        this.birthday = birthday;
        this.skill = skill;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getSkill() {
        return skill;
    }

    public void setSkill(String skill) {
        this.skill = skill;
    }
}
