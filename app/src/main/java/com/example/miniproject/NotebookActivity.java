package com.example.miniproject;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.core.content.ContextCompat;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridLayout;
import android.widget.LinearLayout;

import com.example.miniproject.databinding.ActivityNotebookBinding;

public class NotebookActivity extends AppCompatActivity {
    private ActivityNotebookBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // initialize binding
        binding = ActivityNotebookBinding.inflate(getLayoutInflater());

        // define view
        View view = binding.getRoot();

        // set view into content view
        setContentView(view);

        // event for add new note book
        onButtonAddNotebookClicked();
    }

    private void onButtonAddNotebookClicked() {
        binding.imgBtnAddNote.setOnClickListener(view -> {
            Intent intent = new Intent(NotebookActivity.this, AddNoteBookActivity.class);
            activityResultLauncher.launch(intent);
        });
    }

    ActivityResultLauncher<Intent> activityResultLauncher = registerForActivityResult(
            new ActivityResultContracts.StartActivityForResult(),
            result -> {
                if (result.getResultCode() == Activity.RESULT_OK && result.getData() != null) {

                    Intent intentResult = result.getData();

                    String title = intentResult.getStringExtra("title");
                    String description = intentResult.getStringExtra("description");

                    binding.gridLayoutNotebook.addView(createNewNotebook(title, description));
                }
            }
    );

    private LinearLayout createNewNotebook(String title, String description) {
        Context context = getApplicationContext();

        LinearLayout linearLayout = new LinearLayout(context);
        linearLayout.setOrientation(LinearLayout.VERTICAL);

        // define column_weight = 1 for gridlayout child
        GridLayout.LayoutParams param = new GridLayout.LayoutParams(GridLayout.spec(
                GridLayout.UNDEFINED, GridLayout.FILL, 1f),
                GridLayout.spec(GridLayout.UNDEFINED, GridLayout.FILL, 1f));

        param.height = (int) getResources().getDimension(R.dimen.linear_layout_height);
        linearLayout.setLayoutParams(param);


        int whiteColor = ContextCompat.getColor(context, R.color.white);

        linearLayout.setBackgroundColor(whiteColor);

        AppCompatTextView textViewTitle = new AppCompatTextView(context);
        AppCompatTextView textViewDescription = new AppCompatTextView(context);

        textViewTitle.setLayoutParams(new LinearLayout.LayoutParams
                (ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        textViewDescription.setLayoutParams(new LinearLayout.LayoutParams
                (ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));

        textViewTitle.setText(title);
        textViewDescription.setText(description);

        textViewTitle.setGravity(Gravity.CENTER_HORIZONTAL);
        textViewDescription.setGravity(Gravity.CENTER_HORIZONTAL);

        textViewTitle.setMaxLines(1);
        textViewTitle.setEllipsize(TextUtils.TruncateAt.END);


        textViewTitle.setTypeface(null, Typeface.BOLD);

        linearLayout.addView(textViewTitle);
        linearLayout.addView(textViewDescription);

        return linearLayout;
    }

}