package com.example.miniproject;

import androidx.appcompat.app.AppCompatActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.example.miniproject.databinding.ActivityAddNoteBookBinding;

public class AddNoteBookActivity extends AppCompatActivity {

    private ActivityAddNoteBookBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // initialize binding
        binding = ActivityAddNoteBookBinding.inflate(getLayoutInflater());

        // define view
        View view = binding.getRoot();

        // set view into content view
        setContentView(view);

        // handle button click - save and cancel
        onButtonSaveClicked();
        onButtonCancelClick();
    }

    private void onButtonSaveClicked(){
        binding.btnSaveNote.setOnClickListener(view -> {
            String title = binding.editTextTitle.getText().toString();
            String description = binding.editTextDescription.getText().toString();

            if(isValidNote(title, description)){
                Intent intent = getIntent();
                intent.putExtra("title", title);
                intent.putExtra("description", description);
                setResult(RESULT_OK, getIntent());
                finish();
            }
        });
    }
    private void onButtonCancelClick(){
        binding.btnCancel.setOnClickListener(view -> {
            finish();
        });
    }

    private boolean isValidNote(String title, String description){

        boolean isValid = true;

        if(title.isEmpty()){
            binding.editTextTitle.setError("Title cannot be empty");
            isValid = false;
        }

        if(description.isEmpty()){
            binding.editTextDescription.setError("Description cannot be empty");
            isValid = false;
        }

        return isValid;
    }
}