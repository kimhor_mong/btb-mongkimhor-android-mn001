package com.example.miniproject;

import androidx.appcompat.app.AppCompatActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.example.miniproject.databinding.ActivityMainBinding;

public class MainActivity extends AppCompatActivity {
    private ActivityMainBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // initialize binding
        binding = ActivityMainBinding.inflate(getLayoutInflater());

        // define view
        View view = binding.getRoot();

        // set view into content view
        setContentView(view);

        // methods for handling click event
        onButtonGalleryClicked();
        onButtonPhoneCallClicked();
        onButtonNotebookClicked();
        onButtonProfileClicked();


    }

    private void onButtonGalleryClicked(){
        binding.btnGallery.setOnClickListener(view->{
            Intent intent = new Intent(MainActivity.this, GalleryActivity.class);
            startActivity(intent);
        });
    }

    private void onButtonPhoneCallClicked(){
        binding.btnPhoneCall.setOnClickListener(view->{
            Intent intent = new Intent(MainActivity.this, PhoneCallActivity.class);
            startActivity(intent);
        });
    }

    private void onButtonNotebookClicked(){
        binding.btnNotebook.setOnClickListener(view->{
            Intent intent = new Intent(MainActivity.this, NotebookActivity.class);
            startActivity(intent);
        });
    }

    private void onButtonProfileClicked(){
        binding.btnProfile.setOnClickListener(view -> {
            Intent intent = new Intent(MainActivity.this, ProfileActivity.class);
            startActivity(intent);
        });
    }
}