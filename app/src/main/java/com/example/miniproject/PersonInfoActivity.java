package com.example.miniproject;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.example.miniproject.databinding.ActivityPersonInfoBinding;

public class PersonInfoActivity extends AppCompatActivity {
    private ActivityPersonInfoBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // initialize binding
        binding = ActivityPersonInfoBinding.inflate(getLayoutInflater());

        // define view
        View view = binding.getRoot();

        // set view into content view
        setContentView(view);

        // get and display person info
        Intent intent = getIntent();
        PersonInfo personInfo = (PersonInfo) intent.getSerializableExtra("personInfo");
        displayPersonInfo(personInfo);
    }

    private void displayPersonInfo(PersonInfo personInfo){
        binding.textViewName.setText(personInfo.getName());
        binding.textViewGender.setText(personInfo.getGender());
        binding.textViewBirthday.setText(personInfo.getBirthday());
        binding.textViewSkill.setText(personInfo.getSkill());
    }
}